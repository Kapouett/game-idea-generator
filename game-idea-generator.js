// By Kapouett

// SPDX-License-Identifier: WTFPL
/*
/ This work is free. You can redistribute it and/or modify it under the
/ terms of the Do What The Fuck You Want To Public License, Version 2,
/ as published by Sam Hocevar. See the LICENSE file for more details.
*/

const restrictions = [
	'asymetrical-multiplayer',
	'co-op',
	'fast-paced',
	'monochrome',
	'one-button',
	'pacifist',
	'procedural',
	'retro',
	'sandbox',
	'text-based',
	'textless',
	'turn-based',
	'two-buttons',
];

const settings = [
	'abstract',
	'high fantasy',
	'dark fantasy',
	'pre-historic',
	'antiquity',
	'mythologic',
	'medieval',
	'victorian',
	'pirates',
	'wild west',
	'world war',
	'modern-day',
	'dystopian',
	'sci-fi',
	'steampunk',
	'dieselpunk',
	'post-apocalyptic',
];

const genres = [
	'battle royale',
	'beat \'em up',
	'building',
	'bullet hell',
	'clicker',
	'cooking',
	'dating sim',
	'die & retry',
	'exploration',
	'farming',
	'fighting',
	'hacking',
	'horror',
	'infinite-runner',
	'metroidvania',
	'pinball',
	'platformer',
	'point & click',
	'programming',
	'puzzle',
	'quizz',
	'racing',
	'rhythm',
	'roguelike',
	'rpg',
	'shooter',
	'sports',
	'stealth',
	'strategy',
	'survival',
	'tower defense',
	'typing',
	'visual novel',
];

const details = [
	'cowboys',
	'ninjas',
	'soldiers',
	'knights',
	'ghosts',
	'zombies',
	'demons',
	'animals',
	'insects',
	'birds',
	'cats',
	'dogs',
	'dinosaurs',
	'aliens',
	'anthros',
	'catgirls',
	'catboys',
	'robots',
	'androids',
	'cyborgs',
	'a grappling hook',
	'mind-control',
	'time-travel',
	'teleportation',
	'guns',
	'swords',
	'chocolate',
	'eggs',
	'pasta',
	'fruits',
	'pastries',
	'flowers',
	'trees',
	'magical girls',
	'clowns',
	'nobles',
	'gods',
	'guitars',
	'skeletons',
	'monsters',
	'magnets',
	'paint',
	'electricity',
	'cards',
	'bosses',
	'an ocean',
	'a companion',
	'climbing',
	'dance',
	'verticality',
	'ships',
];



function pick(list, quantity_min=1, quantity_max=1) {
	set = new Set();
	var count = Math.round( quantity_min + Math.random() * (quantity_max-quantity_min) );
	for (var i = 0; i < count; i++) {
		set.add( list[Math.floor(Math.random() * list.length)] );
	}
	return Array.from(set);
}


function nice_join(list) {
	if (list.length === 0)
		return '';
	if (list.length === 1)
		return list[0];
	last = list[list.length-1];
	firsts = list.slice(0, list.length-1);
	return firsts.join(', ') + ' and ' + last;
}


function generate_idea() {
	var restriction = nice_join( pick(restrictions, 0, 1) );
	
	var setting = nice_join( pick(settings) );
	
	var genre = nice_join( pick(genres, 1, 3) );
	
	var detail = nice_join( pick(details, 0, 3) );
	if (detail.length != 0)
		detail = ' with ' + detail;
	
	game_idea = [restriction, setting, genre].filter(function (el) {
		return el.length != 0;
	}).join(' ') + ' game' + detail + '.';
	
	if ( ['a', 'e', 'i', 'o', 'u'].includes(game_idea[0]) )
		game_idea = 'An ' + game_idea;
	else
		game_idea = 'A ' + game_idea;
	
	return game_idea;
}


function generate_ideas_list(list_id, count=10) {
	var list = document.getElementById( list_id );
	
	list.innerHTML = '';
	
	for (var i = 0; i < count; i++) {
		var entry = document.createElement('li');
		entry.appendChild(document.createTextNode( generate_idea() ));
		list.appendChild(entry);
	}
}

