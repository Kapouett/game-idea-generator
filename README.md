# Game Idea Generator

This is a simple pure javascript game idea generator.

Running it [from here](https://kapouett.gitlab.io/game-idea-generator) will generate 10 ideas at a time.

If anyone actually makes a game(s) out of this silly thing please lmk :)

Some outputs:

- `A textless sci-fi rpg game with knights and a grappling hook.`
- `A world war die & retry, fighting and shooter game with androids.`
- `An one-button wild west bullet hell and typing game.`
- `A steampunk dating sim game.`
- `A retro dystopian programming game with flowers.`
- `A monochrome mythologic roguelike and puzzle game with teleportation.`

